#include <stdio.h>
#include <stdlib.h>
#include <time.h>


typedef struct singly_linked_list
{
    int data;
    struct singly_linked_list *next;
} list;


list *new_node(int data)
{
    list *node = malloc(sizeof(list));

    if (node == NULL) {
        puts("Memory allocation failed!");
        exit(1);
    }

    node->data = data;

    return node;
}

void link_node(list **head, list **current, list *node)
{
    if (*head == NULL) {
        *head    = node;
        *current = node;
    } else {
        (*current)->next = node;
        *current         = node;
    }

    (*current)->next = NULL;
}

void print_list(list *node)
{
    if (node == NULL)
        printf("Empty list\n");
    else {
        while (node != NULL) {
            printf("%i ", node->data);
            node = node->next;
        }
        puts("");
    }
}

void delete_list(list **head)
{
    while (*head != NULL) {
        list *temp = (*head)->next;

        free(*head);

        *head = temp;
    }
}

list *merge(list *left, list *right)
{
    list *head    = NULL;
    list *current = NULL;

    while (left != NULL && right != NULL) {
        if (left->data < right->data) {
            list *node = new_node(left->data);
            link_node(&head, &current, node);

            left = left->next;
        } else {
            list *node = new_node(right->data);
            link_node(&head, &current, node);

            right = right->next;
        }
    }

    while (left != NULL) {
        list *node = new_node(left->data);
        link_node(&head, &current, node);

        left = left->next;
    }
    while (right != NULL) {
        list *node = new_node(right->data);
        link_node(&head, &current, node);
        right = right->next;
    }

    return head;
}

list *merge_sort(list *node, int size)
{
    if (size <= 1)
        return node;

    int middle = size / 2;

    list *left  = node;
    list *right = NULL;

    /*
     * i starts from 1 because the starting node is already assigned to left
     *
     * Otherwise, if we start from 0 the middle of list size 4 would be 2+1
     * since first node is not counted as 0, instead, second node is
     *
     * So now we're already in second node
     */
    for (int i = 1; i < middle; i++)
        node = node->next;

    right = node->next; // set right list
    node->next = NULL;  // don't assign to left what's already assigned to right

    printf("\tLeft:\t");
    print_list(left);
    printf("\tRight:\t");
    print_list(right);
    puts("");

    left  = merge_sort(left , middle);
    right = merge_sort(right, size - middle);

    return merge(left, right);
}

int main()
{
    srand(time(NULL));

    printf("Enter list size: ");
    int size = 0;
    scanf("%i", &size);
//    int size = 1000000;

    while (size < 0) {
        puts("");
        printf("Number must be between 0 and %i\n", __INT32_MAX__);
        printf("Try another number: ");
        scanf("%i", &size);
    }

    list *head    = NULL;
    list *current = NULL;

    /*
     * There is no way to get the exact precise time
     *
     * However, there are ways to get at least close to exact time but at the
     * cost of losing portabilty
     * For instance: on Linux <sys/times.h> can be used but that won't go well
     * on other platforms.
     *
     * So it's better sticking to the ones already provided from C standard
     * library for maximum portability
     */
    clock_t start = clock();
    for (int i = 1; i <= size; i++) {
        list *node = new_node(rand() % 10);
        link_node(&head, &current, node);
    }
    clock_t end = clock();

    double time_create = ((double)(end - start)) / CLOCKS_PER_SEC;

    puts("Before sorting:");
    print_list(head);

    start        = clock();
    list *result = merge_sort(head, size);
    end          = clock();

    double time_merge = ((double)(end - start)) / CLOCKS_PER_SEC;

    puts("After sorting:");
    print_list(result);

    delete_list(&head);

    print_list(head);

    puts("-----------------------------------");
    printf("Time estimation for list of size %i\n", size);
    puts("-----------------------------------");
    printf("List created:\t%fs\n", time_create);
    printf("Merge sorted:\t%fs\n", time_merge);

    return 0;
}
